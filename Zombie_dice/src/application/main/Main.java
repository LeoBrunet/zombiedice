package application.main;

import java.io.IOException;

import application.views.JeuController;
import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class Main extends Application {
	
	private Stage primaryStage;
	private AnchorPane home;
	private AnchorPane mainLayout;
	private AnchorPane victory;
	
	@FXML
	private Button tableauScore;

	@Override
	public void start(Stage primaryStage) throws IOException {
		this.primaryStage=primaryStage;
		this.primaryStage.setTitle("ZombieDice");
		afficherHome();
		
		primaryStage.show();

	}

	// Afficher le jeu
	public void afficherJeu() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		JeuController controller = (JeuController) loader.getController();
		loader.setLocation(Main.class.getResource("../views/Jeu.fxml"));
		mainLayout = loader.load();
		Scene scene = new Scene(mainLayout, 1280, 800);
		primaryStage.setScene(scene);
		
	}
	
	public void afficherHome() throws IOException{		
		AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("../views/Home.fxml"));
		root.setId("background");
		Scene scene = new Scene(root,1280,800);
		scene.getStylesheets().add(getClass().getResource("../application.css").toExternalForm());
		primaryStage.setScene(scene);
	}

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void stop() {

	}

	public void setVictory() throws IOException {
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(Main.class.getResource("../views/Victoire.fxml"));
		victory = loader.load();
		Scene scene = new Scene(victory, 400, 400);
		primaryStage.setScene(scene);
	}

}

/*public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			BorderPane root = (BorderPane)FXMLLoader.load(getClass().getResource("Sample.fxml"));
			root.setId("all-wrapper");
			Scene scene = new Scene(root,1000,600);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			primaryStage.setScene(scene);
			primaryStage.setTitle("ZombieDice");
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}*/
