package application.model;

import java.util.ArrayList;
import java.util.List;

public class BoiteD�s extends ConteneurD�s {
	
	private List<D�> D�s;

	public BoiteD�s(Difficult� difficult�) {
		D�s=new ArrayList<D�>();
		int nbD�Vert = 0;
		int nbD�Jaune = 0;
		int nbD�Rouge = 0;
		if(difficult�==difficult�.FACILE) {
			nbD�Vert=8;
			nbD�Jaune=3;
			nbD�Rouge=2;
		}
		if(difficult�==difficult�.MOYEN) {
			nbD�Vert=6;
			nbD�Jaune=4;
			nbD�Rouge=3;
		}
		if(difficult�==difficult�.DIFFICILE) {
			nbD�Vert=4;
			nbD�Jaune=5;
			nbD�Rouge=4;
		}
			
			
		for (int i=0; i<nbD�Vert; i++) {
			D�s.add(new D�(CouleurD�.VERT));
		}
		for (int i=0; i<nbD�Jaune; i++) {
			D�s.add(new D�(CouleurD�.JAUNE));
		}
		for (int i=0; i<nbD�Rouge; i++) {
			D�s.add(new D�(CouleurD�.ROUGE));
		}
	}
	
	public List<D�> getD�s() {
		return D�s;
	}

	public void setD�s(List<D�> d�s) {
		D�s = d�s;
	}
}
