package application.model;


public class Partie {
	private BoiteD�s boiteD�s;
	private Joueurs joueurs;
	
	public Partie(BoiteD�s boiteD�s, Joueurs joueurs) {
		this.joueurs=joueurs;
		this.setBoiteD�s(boiteD�s);
	}
	
	public Joueurs getJoueurs() {
		return joueurs;
	}

	public void setJoueurs(Joueurs joueurs) {
		this.joueurs = joueurs;
	}

	public BoiteD�s getBoiteD�s() {
		return boiteD�s;
	}

	public void setBoiteD�s(BoiteD�s boiteD�s) {
		this.boiteD�s = boiteD�s;
	}

}
