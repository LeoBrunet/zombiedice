package application.model;

import java.util.ArrayList;
import java.util.List;

public class ConteneurD�s {
	private List<D�> D�s;
	
	public ConteneurD�s() {
		D�s=new ArrayList<D�>();
	}
	
	public List<D�> getD�s() {
		return D�s;
	}

	public void setD�s(List<D�> d�s) {
		D�s = d�s;
	}
}
