package application.model;

public class D� {
	private CouleurD� couleurD�;
	private EtatFace etatFace = null;
	private int nbCerveaux;

	public D�(CouleurD� couleurD�) {
		this.couleurD�=couleurD�;
		switch (couleurD�) {
		case VERT:
			nbCerveaux = 3;
			break;
		case JAUNE:
			nbCerveaux = 2;
			break;
		case ROUGE:
			nbCerveaux = 1;
			break;
		}
	}

	public EtatFace lancerD�() {
		EtatFace etatFace = null;
		double random = Math.random() * 6;

		if (random <= nbCerveaux) {
			etatFace = EtatFace.CERVAL;
		} else if (random <= nbCerveaux + 2) {
			etatFace = EtatFace.EMPREINTES;
		} else if (random <= 6) {
			etatFace = EtatFace.FUSIL;
		}
		
		return etatFace;
	}
	
	public String toString() {
		return "D� de couleur "+couleurD�;
	}

	public EtatFace getEtatFace() {
		return etatFace;
	}

	public void setEtatFace(EtatFace etatFace) {
		this.etatFace = etatFace;
	}
	public CouleurD� getCouleurD�() {
		return couleurD�;
	}

	public void setCouleurD�(CouleurD� couleurD�) {
		this.couleurD� = couleurD�;
	}
}
