package application.views;

import java.io.IOException;

import application.model.BoiteD�s;
import application.model.Difficult�;
import application.model.Joueur;
import application.model.Joueurs;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class ParamsController {

	@FXML
	private ComboBox<String> difficulte;

	@FXML
	private ComboBox<Integer> nbJoueur;

	@FXML
	private Label erreur;
	
	@FXML
	private Button jouer;

	@FXML
	private ListView<String> listJoueur;

	@FXML
	private TextField nomJoueur;

	private ObservableList<String> items = FXCollections.observableArrayList();
	private BoiteD�s b;
	private Joueurs joueurs;
	private Difficult� difficulteC;

	@FXML
	private void initialize() {

		erreur.setOpacity(0);
		ObservableList<String> valeur = FXCollections.observableArrayList();
		valeur.add("Facile");
		valeur.add("Moyen");
		valeur.add("Difficile");
		this.difficulte.setItems(valeur);
		difficulte.getSelectionModel().select(0);

		if (nbJoueur != null) {
			for (int i = 0; i < nbJoueur.getValue(); i++) {

			}
		}
	}

	public void add() {
		String text = nomJoueur.getText();
		if (!(text.equals("") || text.equals(" "))) {
			items.add(text);
			listJoueur.setItems(items);
			nomJoueur.clear();
		}
	}

	@FXML
	public void jouer() {
		if (items.size() < 2) {
			erreur.setOpacity(1);
		} else {
			erreur.setOpacity(0);
			if (difficulte.getSelectionModel().getSelectedItem().equals("Difficile"))
				difficulteC = Difficult�.DIFFICILE;
			else if (difficulte.getSelectionModel().getSelectedItem().equals("Moyen"))
				difficulteC = Difficult�.MOYEN;
			else if (difficulte.getSelectionModel().getSelectedItem().equals("Facile"))
				difficulteC = Difficult�.FACILE;
			b = new BoiteD�s(difficulteC);
			joueurs = new Joueurs(items.size());
			for (int i = 0; i < items.size(); i++) {
				Joueur joueur = new Joueur(items.get(i), b);
				joueurs.getJoueurs().add(joueur);
			}
			try {
				FXMLLoader loader = new FXMLLoader(getClass().getResource("Jeu.fxml"));
				JeuController controller = new JeuController(joueurs, difficulteC);
				loader.setController(controller);
				Parent page3 = loader.load();
				Scene jPage3 = new Scene(page3);
				Stage jPage3_Stage = (Stage) jouer.getScene().getWindow();
				System.out.println(jPage3);
				System.out.println(jPage3_Stage);
				jPage3.getStylesheets().add(getClass().getResource("../application.css").toExternalForm());
				jPage3_Stage.setScene(jPage3);
				jPage3_Stage.show();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
