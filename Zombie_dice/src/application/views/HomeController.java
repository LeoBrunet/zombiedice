package application.views;

import java.io.IOException;

import application.main.Main;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class HomeController {

	@FXML
	private ImageView playButton;

	@FXML
	public void initialize() {

	}

	@FXML
	public void play(MouseEvent event) {
		playButton.setScaleX(1);
		playButton.setScaleY(1);
		try {
			AnchorPane root = (AnchorPane)FXMLLoader.load(getClass().getResource("ParamsController.fxml"));
			root.setId("background");
			Scene jPage = new Scene(root,1280,800);
			jPage.getStylesheets().add(getClass().getResource("../application.css").toExternalForm());
			Stage jPage_Stage = (Stage) playButton.getScene().getWindow();
			jPage_Stage.setScene(jPage);
			jPage_Stage.show();
		

		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

	@FXML
	void hoverButton() {
		playButton.setScaleX(1.1);
		playButton.setScaleY(1.1);
	}

	@FXML
	void leaveButton() {
		playButton.setScaleX(1);
		playButton.setScaleY(1);
	}
}
