package application.views;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import application.main.Main;
import application.model.*;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class JeuController {

	@FXML
	private AnchorPane root;
	private BoiteD�s b;
	private Joueurs joueurs;
	private boolean dernierTour = false;
	private Difficult� difficulte;
	private Joueur joueurActif;
	int numJoueur = 1;

	/* IMAGES */
	private Image cerveauVert = new Image("file:ressources/cerveau_vert.png", 100, 100, false, false);
	private Image cerveauRouge = new Image("file:ressources/cerveau_rouge.png", 100, 100, false, false);
	private Image cerveauJaune = new Image("file:ressources/cerveau_jaune.png", 100, 100, false, false);
	private Image empreintesJaune = new Image("file:ressources/empreintes_jaune.png", 100, 100, false, false);
	private Image empreintesVert = new Image("file:ressources/empreintes_vert.png", 100, 100, false, false);
	private Image empreintesRouge = new Image("file:ressources/empreintes_rouge.png", 100, 100, false, false);
	private Image fusilsJaune = new Image("file:ressources/fusils_jaune.png", 100, 100, false, false);
	private Image fusilsVert = new Image("file:ressources/fusils_vert.png", 100, 100, false, false);
	private Image fusilsRouge = new Image("file:ressources/fusils_rouge.png", 100, 100, false, false);
	/* LABEL */
	@FXML
	private Label nomJoueurAff;
	@FXML
	private Label nbCerveauxTotAff;
	@FXML
	private Label winner;

	/* BUTTONS */
	@FXML
	private Button lancer;
	@FXML
	private Button passer;
	@FXML
	private Button tableauScore;
	/* CANVAS */
	@FXML
	private Canvas canvas;
	private GraphicsContext gc;

	@FXML
	private Canvas fusils;
	private GraphicsContext gc2;

	@FXML
	private GridPane cerveaux;

	public JeuController(Joueurs joueurs, Difficult� difficulte) {
		this.joueurs = joueurs;
		this.difficulte = difficulte;
	}

	@FXML
	public void initialize() throws IOException {

		root.setId("background");
		gc = canvas.getGraphicsContext2D();
		gc2 = fusils.getGraphicsContext2D();
		b = new BoiteD�s(difficulte);
		joueurActif = joueurs.getJoueurs().get(0);

		nomJoueurAff.setText(joueurActif.getNom());
		tableauScore.setVisible(false);
		winner.setVisible(false);

		lancer.setOnMouseClicked(event -> {

			System.out.println("\n DernierTour : " + dernierTour);
			List<D�> d�d� = joueurActif.lancerD�s();
			afficherD�s(d�d�);
			afficherCerveaux(joueurActif.getCerveaux());
			afficherFusils(joueurActif.getFusils());
//			nbCerveauxAff.setText("Nombre de cerveaux : " + joueurActif.getNbCerveaux());
			nbCerveauxTotAff.setText("" + joueurActif.getNbCerveauxTot());
			if (joueurActif.troisFusils()) {
				System.out.println("vraiment pas de chance");
				lancer.setVisible(false);
			}
			if (joueurActif.isGagner()) {
				joueurActif.setVainqueur(true);
				// joueurActif.finDeTour();
				suivant();
			}

		});

		passer.setOnMouseClicked(event -> {
			joueurActif.finDeTour();
			if (joueurActif.isGagner()) {
				joueurActif.setVainqueur(true);
				suivant();
			} else {
				suivant();
			}
		});

		tableauScore.setOnMouseClicked(e -> {

			FXMLLoader loader = new FXMLLoader(getClass().getResource("../views/Victoire.fxml"));
			List<String> noms = new ArrayList<String>();
			List<String> scores = new ArrayList<String>();
			Collections.sort(joueurs.getJoueurs(), Collections.reverseOrder());
			for (int i = 0; i < joueurs.getJoueurs().size(); i++) {
				noms.add(joueurs.getJoueurs().get(i).getNom());
				scores.add("" + joueurs.getJoueurs().get(i).getNbCerveauxTot());
			}
			VictoryController controller = new VictoryController(noms, scores, verifWin());
			loader.setController(controller);
			Parent page3 = null;
			try {
				page3 = loader.load();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			Scene jPage3 = new Scene(page3);
			jPage3.getStylesheets().add(getClass().getResource("../application.css").toExternalForm());
			Stage jPage3_Stage = (Stage) tableauScore.getScene().getWindow();
			jPage3_Stage.setScene(jPage3);
			jPage3_Stage.show();

			/*
			 * FXMLLoader loader = new
			 * FXMLLoader(getClass().getResource("../views/Home.fxml")); Parent page4 =
			 * null; try { page4 = loader.load(); } catch (IOException e1) { // TODO
			 * Auto-generated catch block e1.printStackTrace(); } Scene scene = new
			 * Scene(page4,1280,800); Stage stage = (Stage)
			 * tableauScore.getScene().getWindow();
			 * scene.getStylesheets().add(getClass().getResource("../application.css").
			 * toExternalForm());
			 * 
			 * List<String> noms = new ArrayList<String>(); List<String> scores = new
			 * ArrayList<String>(); System.out.println(joueurs.getJoueurs().toString());
			 * Collections.sort(joueurs.getJoueurs(),Collections.reverseOrder());
			 * System.out.println(joueurs.getJoueurs().toString()); for(int i=0;
			 * i<joueurs.getJoueurs().size();i++) {
			 * noms.add(joueurs.getJoueurs().get(i).getNom());
			 * scores.add(""+joueurs.getJoueurs().get(i).getNbCerveauxTot()); }
			 * System.out.println(noms); System.out.println(scores); VictoryController
			 * controller = new VictoryController(noms, scores, verifWin());
			 * loader.setController(controller); stage.setScene(scene); stage.show();
			 */

		});

	}

	private void afficherCerveaux(List<D�> cerveaux2) {
		int row = 0;
		int column = 0;
		for (int i = 0; i < cerveaux2.size(); i++) {
			D� d = cerveaux2.get(i);
			if (d.getCouleurD�() == CouleurD�.VERT) {
				cerveaux.add(new ImageView(this.cerveauVert), column, row);
			} else if (d.getCouleurD�() == CouleurD�.JAUNE) {
				cerveaux.add(new ImageView(this.cerveauJaune), column, row);
			} else if (d.getCouleurD�() == CouleurD�.ROUGE) {
				cerveaux.add(new ImageView(this.cerveauRouge), column, row);
			}
			if (row < 6) {
				row++;
			} else {
				row = 0;
				column = 1;
			}
		}

	}

	private void afficherFusils(List<D�> fusils) {
		System.out.println("nb fusils :" + fusils.size());
		for (int i = 0; i < fusils.size(); i++) {
			System.out.println(i);
			D� d = fusils.get(i);
			if (d.getCouleurD�() == CouleurD�.VERT) {
				gc2.drawImage(this.fusilsVert, (130 * i), 0);
			} else if (d.getCouleurD�() == CouleurD�.JAUNE) {
				gc2.drawImage(this.fusilsJaune, (130 * i), 0);
			} else if (d.getCouleurD�() == CouleurD�.ROUGE) {
				gc2.drawImage(this.fusilsRouge, (130 * i), 0);
			}
		}
	}

	private void afficherD�s(List<D�> lancerD�s) {
		for (int i = 0; i < lancerD�s.size(); i++) {
			D� d = lancerD�s.get(i);
			System.out.println(i);
			if (d.getCouleurD�() == CouleurD�.VERT) {
				if (d.getEtatFace() == EtatFace.EMPREINTES) {
					gc.drawImage(this.empreintesVert, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.CERVAL) {
					gc.drawImage(this.cerveauVert, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.FUSIL) {
					gc.drawImage(this.fusilsVert, (130 * i), 0);
				}
			} else if (d.getCouleurD�() == CouleurD�.JAUNE) {
				if (d.getEtatFace() == EtatFace.EMPREINTES) {
					gc.drawImage(this.empreintesJaune, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.CERVAL) {
					gc.drawImage(this.cerveauJaune, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.FUSIL) {
					gc.drawImage(this.fusilsJaune, (130 * i), 0);
				}
			} else if (d.getCouleurD�() == CouleurD�.ROUGE) {
				if (d.getEtatFace() == EtatFace.EMPREINTES) {
					gc.drawImage(this.empreintesRouge, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.CERVAL) {
					gc.drawImage(this.cerveauRouge, (130 * i), 0);
				}
				if (d.getEtatFace() == EtatFace.FUSIL) {
					gc.drawImage(this.fusilsRouge, (130 * i), 0);
				}
			}
		}

	}

	private String verifWin() {
		if (joueurActif.isVainqueur()) {
			lancer.setVisible(false);
			passer.setVisible(false);
			winner.setText("Le vainqueur est donc " + joueurActif.getNom());
			winner.setVisible(true);
			tableauScore.setVisible(true);
			return joueurActif.getNom();
		}
		return null;
	}

	public void suivant() {

		lancer.setVisible(true);
		joueurActif.finDeTour();
		if (numJoueur >= joueurs.getJoueurs().size()) {
			numJoueur = 0;
		}

		joueurActif = joueurs.getJoueurs().get(numJoueur);
		nomJoueurAff.setText(joueurActif.getNom());
		nbCerveauxTotAff.setText("" + joueurActif.getNbCerveauxTot());

		numJoueur++;

		verifWin();
		gc.clearRect(0, 0, 360, 100);
		gc2.clearRect(0, 0, 360, 100);
		while (cerveaux.getChildren().size() > 0) {
			cerveaux.getChildren().remove(0);
		}
	}
}
