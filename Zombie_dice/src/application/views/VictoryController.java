package application.views;

import java.util.ArrayList;
import java.util.List;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;

public class VictoryController {
	
	@FXML
	private AnchorPane root2;
	@FXML
	private Label winnerAff;
	
	@FXML
	private ListView<String> nomJoueurs;
	private ObservableList<String> items = FXCollections.observableArrayList();
	
	@FXML
	private ListView<String> scoreJoueurs;
	private ObservableList<String> scores = FXCollections.observableArrayList();
	
	private ArrayList<String> nomsJoueurs;
	private ArrayList<String> scoresJoueurs;
	private String nomVainqueur;
	
	public VictoryController(List<String> noms, List<String> scores, String nom) {
		this.nomsJoueurs=(ArrayList<String>) noms;
		this.scoresJoueurs=(ArrayList<String>) scores;
		this.nomVainqueur=nom;
	}
	
	@FXML
	public void initialize() {
		root2.setId("background");
		this.items.addAll(nomsJoueurs);
		this.nomJoueurs.setItems(this.items);
		this.scores.addAll(scoresJoueurs);
		this.scoreJoueurs.setItems(this.scores);
		winnerAff.setText("Bravo "+nomVainqueur+" !" );
		System.out.println(nomsJoueurs);
	}
}
